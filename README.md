# README #

Accompanying source code for blog entry at https://tech.asimio.net/2021/05/19/Fixing-Hibernate-HHH000104-firstResult-maxResults-warning-using-Spring-Data-JPA.html


### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/films?page=0&size=10"
```
should result in successful responses. Please look at the logs to verify:
HHH000104: firstResult/maxResults specified with collection fetch; applying in memory! warning message
is not logged.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
