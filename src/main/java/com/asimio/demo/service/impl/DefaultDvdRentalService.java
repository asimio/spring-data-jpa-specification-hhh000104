package com.asimio.demo.service.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.asimio.demo.dao.FilmDao;
import com.asimio.demo.dao.filter.FilmSpecifications;
import com.asimio.demo.domain.Film;
import com.asimio.demo.service.DvdRentalService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class DefaultDvdRentalService implements DvdRentalService {

    private final FilmDao filmDao;

    @Override
    public Page<Film> retrieveFilms(Pageable page) {
        // Getting film ids and page data to prevent:
        // HHH000104: firstResult/maxResults specified with collection fetch; applying in memory! 
        // which affects application's performance
        Page<Integer> filmIdsPage = this.filmDao.findEntityIds(page);

        List<Film> result;
        List<Integer> filmIds = filmIdsPage.getContent();
        if (CollectionUtils.isEmpty(filmIds)) {
            result = Lists.newArrayList();
        } else {
            // Retrieve films using IN predicate
            Specification<Film> fimlIdInSpecification = FilmSpecifications.idIn(Sets.newHashSet(filmIds));
            result = this.filmDao.findAll(fimlIdInSpecification);
        }
        return PageableExecutionUtils.getPage(result, page, () -> filmIdsPage.getTotalElements());

        // Causes HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!
        // return this.filmDao.findAll(page);
    }
}