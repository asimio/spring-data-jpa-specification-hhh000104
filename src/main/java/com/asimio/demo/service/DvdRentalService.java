package com.asimio.demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.asimio.demo.domain.Film;

public interface DvdRentalService {

    Page<Film> retrieveFilms(Pageable page);
}