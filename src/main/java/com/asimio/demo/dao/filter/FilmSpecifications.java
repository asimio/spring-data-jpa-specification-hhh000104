package com.asimio.demo.dao.filter;

import java.util.Set;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import com.asimio.demo.domain.Film;
import com.asimio.demo.domain.Film_;

public final class FilmSpecifications {

    private FilmSpecifications() {
    }

    public static Specification<Film> idIn(Set<Integer> filmIds) {
        if (CollectionUtils.isEmpty(filmIds)) {
            return null;
        }
        return (root, query, builder) -> root.get(Film_.filmId).in(filmIds);
    }
}