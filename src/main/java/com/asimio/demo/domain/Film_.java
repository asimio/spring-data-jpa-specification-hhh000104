package com.asimio.demo.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Film.class)
public class Film_ {

    public static volatile SingularAttribute<Film, Integer> filmId;
}