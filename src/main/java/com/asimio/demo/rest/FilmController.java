package com.asimio.demo.rest;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asimio.demo.domain.Film;
import com.asimio.demo.rest.mapper.FilmResourceMapper;
import com.asimio.demo.rest.model.FilmResource;
import com.asimio.demo.service.DvdRentalService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/films", produces = MediaType.APPLICATION_JSON_VALUE)
public class FilmController {

    private static final int DEFAULT_PAGE_NUMBER = 0;
    private static final int DEFAULT_PAGE_SIZE = 20;

    private final DvdRentalService dvdRentalService;

    @GetMapping(path = "")
    public ResponseEntity<List<FilmResource>> retrieveFilms(
            @PageableDefault(page = DEFAULT_PAGE_NUMBER, size = DEFAULT_PAGE_SIZE) Pageable page) {

        Page<Film> films = this.dvdRentalService.retrieveFilms(page);
        List<FilmResource> result = FilmResourceMapper.INSTANCE.map(films.getContent());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}